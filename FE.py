import meshpy.triangle as triangle
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as pt
import matplotlib.tri as tri
import matplotlib.collections
from scipy.interpolate import griddata

# INPUTS
maxElementVol = 0.005 # Maximum element area in the mesh
minElementAngle = 0 # Minimum element angle in the mesh
refinementDist = 0.15 # Radial distance from concave vertex defining refinement area
refinementArea = 0.001 # Element size for refinement in near-concave vertex region
filename = 'POLY_data.txt' # Input text file name
E = 1000 # Young's Modulus
nu = 0.3 # Poisson's Ratio
t = 1 # element thickness


class element:
    # An element in the mesh, constant strain triangle (CST)

    def __init__(self, nodes, globalIndex1, globalIndex2, globalIndex3):
        self.nodes = nodes # The element node coordinates in the global coordinate system
        self.globalIndex1 = globalIndex1 # The indicies of the nodes in the global numbering
        self.globalIndex2 = globalIndex2
        self.globalIndex3 = globalIndex3
        self.centroidX = (self.nodes[0,0] + self.nodes[1,0] + self.nodes[2,0]) / 3
        self.centroidY = (self.nodes[0,1] + self.nodes[1,1] + self.nodes[2,1]) / 3

    def elementStiffness(self):
        # Calculates the element stiffness matrix

        # Geometry of CST
        self.x12 = self.nodes[0,0] - self.nodes[1,0]
        self.x13 = self.nodes[0,0] - self.nodes[2,0]
        self.x23 = self.nodes[1,0] - self.nodes[2,0]
        self.y12 = self.nodes[0,1] - self.nodes[1,1]
        self.y13 = self.nodes[0,1] - self.nodes[2,1]
        self.y23 = self.nodes[1,1] - self.nodes[2,1]
        self.x21 = -self.x12
        self.x31 = -self.x13
        self.x32 = -self.x23
        self.y21 = -self.y12
        self.y31 = -self.y13
        self.y32 = -self.y23

        self.detJ = self.x13*self.y23 - self.y13*self.x23 # Jacobian determinant
        self.A = 0.5*abs(self.detJ) # Element area
        self.B = (1/(self.detJ))*np.array([[self.y23, 0, self.y31, 0, self.y12, 0],
                                           [0, self.x32, 0, self.x13, 0, self.x21],
                                           [self.x32, self.y23, self.x13, self.y31, self.x21, self.y12]]) # The B matrix (derivative of shape function matrix), relates the 3 strains to the 6 nodal displacements
        self.D = (E/(1 - nu**2))*np.array([[1, nu, 0],
                                           [nu, 1, 0],
                                           [0, 0, (1 - nu)/2]]) # The D matrix, contains material properties

        self.kel = t * self.A * self.B.T @ self.D @ self.B # Element stiffness matrix

        return self.kel

class meshMod:
    # Creates a meshMod object (called meshMod to avoid confusion with the mesh object of pymesh)
    # Uses the PyMesh library

    def __init__(self, verticies, concavity, displacements, forces):
        self.verticies = verticies # Coordinates of the pentagon verticies
        self.concavity = concavity # concavity = 1 for concave polygon, = 0 for convex polygon
        self.displacements = displacements # len = 10, displacement BCs for the 10 DoFs for the 5 verticies, read in from text file, "1" means fixed (ie zero displacement in that DoF), "0" means not fixed
        self.forces = forces # len = 10. applied vertex force BCs for the 10 DoFs for the 5 verticies
        if self.concavity == 1:
            self.concaveVertex = findConcavePoint(self.verticies) # if the polygon is concave, find which vertex is concave

        # Builds the pymesh.mesh object, includes a refinement function if the polygon is concave
        info = triangle.MeshInfo()
        info.set_points(self.verticies)
        info.set_facets(round_trip_connect(0, len(self.verticies) - 1))
        if concavity == 0:
            self.mesh = triangle.build(info, max_volume=maxElementVol, min_angle=minElementAngle)
        if concavity == 1:
            self.mesh = triangle.build(info, max_volume=maxElementVol, min_angle=minElementAngle, refinement_func=needs_refinement(self.concaveVertex))

        # The first 5 points in mesh_points are the original pentagon verticies, in the same order they were input
        self.mesh_points = np.array(self.mesh.points) # The coordinates of each point in the mesh
        self.mesh_tris = np.array(self.mesh.elements) # The indicies in self.mesh_points for the nodes in each element
        self.numNodes = len(self.mesh_points) # number of nodes in the mesh
        self.numElements = len(self.mesh_tris) # number of elements in the mesh

        self.makeElements() # generate the elements in the mesh
        self.globalStiffness() # assemble the global stiffness matrix
        self.applyBCs() # apply the displacement and force BCs and reduce the global stiffness matrix
        self.solveDisplacements() # solve for global nodal displacements (KU = F)
        self.solveStresses() # solve for the element von Mises stresses
        self.extrapolateNodalStresses() # extrapolate element stress values to nodal values

    def showMesh(self):
        # Displays the mesh

        pt.triplot(self.mesh_points[:, 0], self.mesh_points[:, 1], self.mesh_tris)
        pt.show()
        print(self.mesh_points.size)

    def makeElements(self):
        # Creates the elements in the mesh

        self.elements = []
        for i in range(len(self.mesh_tris)):
            globalIndex1, globalIndex2, globalIndex3 = self.mesh_tris[i] # The global node indicies of the nodes of each mesh element
            self.elements.append(element(np.array([self.mesh_points[globalIndex1], self.mesh_points[globalIndex2], self.mesh_points[globalIndex3]]), globalIndex1, globalIndex2, globalIndex3))

    def globalStiffness(self):
        # Assembles the global stiffness matrix

        # The global stiffness matrix has size of (#Dof x #Dof), the number of DoF is #nodes*2 (x and y)
        self.kGlobal = np.zeros(shape=(2*self.numNodes, 2*self.numNodes))
        for el in self.elements:
            # Make shorthand terms for the global nodal indicies for the nodes in the element
            ind1, ind2, ind3 = el.globalIndex1, el.globalIndex2, el.globalIndex3

            # Add the parts of the element stiffness matrix to the corresponding location in the global stiffness matrix
            self.kGlobal[(ind1)*2:(ind1)*2+2, (ind1)*2:(ind1)*2+2] = self.kGlobal[(ind1)*2:(ind1)*2+2, (ind1)*2:(ind1)*2+2] + el.elementStiffness()[0:1+1, 0:1+1]
            self.kGlobal[(ind1)*2:(ind1)*2+2, (ind2)*2:(ind2)*2+2] = self.kGlobal[(ind1)*2:(ind1)*2+2, (ind2)*2:(ind2)*2+2] + el.elementStiffness()[0:1+1, 2:3+1]
            self.kGlobal[(ind1)*2:(ind1)*2+2, (ind3)*2:(ind3)*2+2] = self.kGlobal[(ind1)*2:(ind1)*2+2, (ind3)*2:(ind3)*2+2] + el.elementStiffness()[0:1+1, 4:5+1]
            self.kGlobal[(ind2)*2:(ind2)*2+2, (ind1)*2:(ind1)*2+2] = self.kGlobal[(ind2)*2:(ind2)*2+2, (ind1)*2:(ind1)*2+2] + el.elementStiffness()[2:3+1, 0:1+1]
            self.kGlobal[(ind2)*2:(ind2)*2+2, (ind2)*2:(ind2)*2+2] = self.kGlobal[(ind2)*2:(ind2)*2+2, (ind2)*2:(ind2)*2+2] + el.elementStiffness()[2:3+1, 2:3+1]
            self.kGlobal[(ind2)*2:(ind2)*2+2, (ind3)*2:(ind3)*2+2] = self.kGlobal[(ind2)*2:(ind2)*2+2, (ind3)*2:(ind3)*2+2] + el.elementStiffness()[2:3+1, 4:5+1]
            self.kGlobal[(ind3)*2:(ind3)*2+2, (ind1)*2:(ind1)*2+2] = self.kGlobal[(ind3)*2:(ind3)*2+2, (ind1)*2:(ind1)*2+2] + el.elementStiffness()[4:5+1, 0:1+1]
            self.kGlobal[(ind3)*2:(ind3)*2+2, (ind2)*2:(ind2)*2+2] = self.kGlobal[(ind3)*2:(ind3)*2+2, (ind2)*2:(ind2)*2+2] + el.elementStiffness()[4:5+1, 2:3+1]
            self.kGlobal[(ind3)*2:(ind3)*2+2, (ind3)*2:(ind3)*2+2] = self.kGlobal[(ind3)*2:(ind3)*2+2, (ind3)*2:(ind3)*2+2] + el.elementStiffness()[4:5+1, 4:5+1]

    def applyBCs(self):
        # apply the displacement and force BCs and reduce the global stiffness matrix

        self.fixedDOF = [] # The DoF indicies of the fixed DoFs
        self.forcesGlobal = np.zeros(shape=(2*self.numNodes, 1)) # The nodal forces matrix is a vector with length = #DoF
        for i in range(10):
            self.forcesGlobal[i, 0] = self.forces[i]
            if self.displacements[i] == 1:
                self.fixedDOF.append(i)

        # Find the global node indicies of the two fixed verticies
        self.fixedNodes = []
        for i in self.fixedDOF:
            self.fixedNodes.append(int((i - i % 2) / 2))
            self.fixedNodes = list(dict.fromkeys(self.fixedNodes))

        # The x and y coords of each fixed node
        x = self.mesh_points[:, 0]
        y = self.mesh_points[:, 1]

        xFixed1 = x[self.fixedNodes[0]]
        yFixed1 = y[self.fixedNodes[0]]
        xFixed2 = x[self.fixedNodes[1]]
        yFixed2 = y[self.fixedNodes[1]]

        # Fix the edge between the two fixed nodes in both x and y
        slope = (yFixed2-yFixed1)/(xFixed2-xFixed1)
        for i in range(self.numNodes):
            if(abs(y[i]-yFixed1 - slope*(x[i]-xFixed1)) < 0.001):
                self.fixedDOF.append(i*2)
                self.fixedDOF.append(i*2+1)
        self.fixedDOF = list(dict.fromkeys(self.fixedDOF))

        # Reduce the global stiffness matrixies and the forces vector in the fixed DoFs
        self.kGlobal = np.delete(self.kGlobal, self.fixedDOF, 0)
        self.kGlobal = np.delete(self.kGlobal, self.fixedDOF, 1)
        self.forcesGlobal = np.delete(self.forcesGlobal, self.fixedDOF, 0)

    def solveDisplacements(self):
        # Solve for the nodal displacements (KU = F)

        self.displacementsGlobal = la.inv(self.kGlobal) @ self.forcesGlobal

        # Reinsert the fixed DoFs as zeros
        self.displacementsGlobalFull = np.zeros(shape=(2*self.numNodes, 1))
        self.forcesGlobalFull = np.zeros(shape=(2*self.numNodes, 1))
        count = 0
        for i in range(self.numNodes*2):
            if i in self.fixedDOF:
                self.displacementsGlobalFull[i] = 0
                self.forcesGlobalFull[i] = 0
            else:
                self.displacementsGlobalFull[i] = self.displacementsGlobal[count]
                self.forcesGlobalFull[i] = self.forcesGlobal[count]
                count = count + 1

        self.displacementsGlobal = self.displacementsGlobalFull
        self.forcesGlobal = self.forcesGlobalFull

    def solveStresses(self):
        # Solve for the von Mises stress in each element

        self.sigma = np.empty(len(self.elements))
        for i in range(len(self.elements)):
            displacementsElement = self.displacementsGlobal[[self.elements[i].globalIndex1*2, self.elements[i].globalIndex1*2+1, self.elements[i].globalIndex2*2, self.elements[i].globalIndex2*2+1, self.elements[i].globalIndex3*2, self.elements[i].globalIndex3*2+1]] # Get the nodal displacements for the element's nodes
            [sig_xx, sig_yy, sig_xy] = self.elements[i].D @ self.elements[i].B @ displacementsElement
            self.sigma[i] = (sig_xx**2 + sig_yy**2 - sig_xx * sig_yy + 3 * sig_xy**2)**0.5

    def extrapolateNodalStresses(self):
        # Extrapolate element stress values to nodal values

        #self.wGlobal = np.zeros(shape=(self.numNodes, self.numNodes))
        #self.rGlobal = np.zeros(shape=(self.numNodes,1))

        #for i in range(len(self.elements)):
        #    # Make shorthand terms for the global nodal indicies for the nodes in the element
        #    el = self.elements[i]
        #    ind1, ind2, ind3 = el.globalIndex1, el.globalIndex2, el.globalIndex3

        #    # Add the parts of the element w and r matricies to the corresponding locations in the global w and r matricies
        #    self.wGlobal[ind1,ind1] = self.wGlobal[ind1,ind1] + (el.A/12) * 2
        #    self.wGlobal[ind2,ind1] = self.wGlobal[ind2,ind1] + (el.A/12) * 1
        #    self.wGlobal[ind3,ind1] = self.wGlobal[ind3,ind1] + (el.A/12) * 1
        #    self.wGlobal[ind1,ind2] = self.wGlobal[ind1,ind2] + (el.A/12) * 1
        #    self.wGlobal[ind2,ind2] = self.wGlobal[ind2,ind2] + (el.A/12) * 2
        #    self.wGlobal[ind3,ind2] = self.wGlobal[ind3,ind2] + (el.A/12) * 1
        #    self.wGlobal[ind1,ind3] = self.wGlobal[ind1,ind3] + (el.A/12) * 1
        #    self.wGlobal[ind2,ind3] = self.wGlobal[ind1,ind3] + (el.A/12) * 1
        #    self.wGlobal[ind3,ind3] = self.wGlobal[ind1,ind3] + (el.A/12) * 2

        #    self.rGlobal[ind1] = self.rGlobal[ind1] + (self.sigma[i] * el.A / 3) * 1
        #    self.rGlobal[ind2] = self.rGlobal[ind2] + (self.sigma[i] * el.A / 3) * 1
        #    self.rGlobal[ind3] = self.rGlobal[ind3] + (self.sigma[i] * el.A / 3) * 1

        #self.sigmaNodal = la.inv(self.wGlobal) @ self.rGlobal # The interpolated von Mises stress at each node (global indexing)

        centroids = np.zeros(shape=(self.numElements, 2))
        for i in range(len(self.elements)):
            centroids[i,0] = self.elements[i].centroidX
            centroids[i,1] = self.elements[i].centroidY

        #f = interp2d(centroids[:,0],centroids[:,1],self.sigma, kind='linear')
        #self.sigmaNodal = f(self.mesh_points[:,0],self.mesh_points[:,1])

        self.sigmaNodal = griddata(centroids, self.sigma, self.mesh_points, method='nearest')


    def showStresses(self):
        # Displays the von Mises stresses

        x = self.mesh_points[:, 0]
        y = self.mesh_points[:, 1]

        def quatplot(x, y, triangles, values, ax=None, **kwargs):

            if not ax: ax=pt.gca()
            xy = np.c_[x, y]
            verts= xy[triangles]
            pc = matplotlib.collections.PolyCollection(verts, **kwargs)
            pc.set_array(values)
            ax.add_collection(pc)
            ax.autoscale()
            return pc

        fig, ax = pt.subplots()
        ax.set_aspect('equal')

        pc = quatplot(x, y, np.asarray(self.mesh_tris), self.sigma, ax=ax,
                 edgecolor="black", cmap="rainbow")
        fig.colorbar(pc, ax=ax)

        #for plotting nodes
        #ax.plot(x, y, marker="o", ls="", color="black")

        pt.show()


        ##Tricontourf Version
        #pt.tricontourf(self.mesh_points[:, 0], self.mesh_points[:, 1], self.mesh_tris, self.sigma)
        #pt.show()

    def showStressesInterpolated(self):
        print(self.sigmaNodal.size)
        triangulation = tri.Triangulation(self.mesh_points[:, 0], self.mesh_points[:, 1], self.mesh_tris)
        pt.tricontourf(triangulation, self.sigmaNodal, levels=8, cmap=pt.cm.jet)
        pt.gca().set_axis_off()
        pt.margins(0,0)
        # my_dpi = 96
        # pt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
        # pt.savefig('mesh1.png', dpi=30)
        # pt.show()


def readPOLY(filename, line_start):
    # Read in the input text file

    file = open(filename)
    verticies = []
    displacements = []
    forces = []
    try:
        lines = file.readlines()
        concavity = int(lines[line_start].split()[1])
        for i in range(1, 6):
            parsed = [float(x) for x in lines[line_start+i].split()]
            verticies.append(np.array([parsed[4], parsed[5]]))
            displacements.extend((parsed[6], parsed[7]))
            forces.extend((parsed[8], parsed[9]))
    finally:
        file.close()
    return verticies, concavity, displacements, forces

def findConcavePoint(points):
    # Returns the coordinates of the concave vertex
    # Assumes the points are ordered CCW

    # If the cross product between adjacent edges is negative, then the path is turning right (ie concave)
    edges = np.diff(points, axis=0)
    edges = np.append(edges, np.reshape(points[0] - points[-1], (1, 2)), axis=0)
    for i in range(len(edges)-1):
        if np.cross(edges[i], edges[i+1]) < 0:
            concavePoint = points[i+1]
    if np.cross(edges[-1], edges[0]) < 0:
        concavePoint = points[0]
    return concavePoint

def round_trip_connect(start, end):
    result = []
    for i in range(start, end):
        result.append((i, i + 1))
    result.append((end, start))
    return result

def needs_refinement(concaveVertex):
    # Defines the refinement criteria

    max_dist = 0

    def refinementInner(verticies, area):
        point1, point2, point3 = verticies
        nodes = np.array([[verticies[0].x, verticies[0].y],
                          [verticies[1].x, verticies[1].y],
                          [verticies[2].x, verticies[2].y]])
        max_dist = max(la.norm(nodes - np.array(concaveVertex), axis=1))
        return bool(max_dist < refinementDist and area > refinementArea)

    return refinementInner


### MAIN
if __name__ == "__main__":
    # for i in range(1, 5000):
    i = 0

    points, concavity, displacements, forces = readPOLY(filename, i*6)

    #print(points)
    #print(concavity)
    #nodes = np.array([[1, 2], [1, 3], [1, 4]])
    #print(la.norm(nodes - np.array([1, 3]), axis=1))
    mesh = meshMod(points, concavity, displacements, forces)
    # mesh.showMesh()
    # mesh.showStressesInterpolated()

    # print(self.sigmaNodal)
    pt.figure(figsize=(10,10))
    triangulation = tri.Triangulation(mesh.mesh_points[:, 0], mesh.mesh_points[:, 1], mesh.mesh_tris)

    pt.tricontourf(triangulation, mesh.sigmaNodal, levels=8, cmap=pt.cm.binary)
    pt.gca().set_axis_off()
    pt.margins(0,0)

    pt.savefig('mesh_%d.png'%i,dpi=15)
    pt.close()
    # pt.show()
    print(i)
