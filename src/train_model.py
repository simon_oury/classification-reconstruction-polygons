from load_data import MyOwnDataset
import torch
from torch.nn import Linear
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool
from torch_geometric.data import DataLoader
from tqdm import tqdm
import matplotlib.pyplot as plt
import os

print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
dataset = dataset.shuffle()
train_dataset = dataset[:4500]
test_dataset = dataset[4500:]
print("Training dataset length: ", len(train_dataset), "\nTesting dataset length: ", len(test_dataset))
print()

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=False)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)
print(f'Number of training batches created: {len(train_loader)}')
print()

class GCN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GCN, self).__init__()
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, 32)
        self.conv3 = GCNConv(32, 20)
        self.lin1 = Linear(20, 20)
        self.lin2 = Linear(20, dataset.num_classes)


    def forward(self, x, edge_index, batch):
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = self.conv2(x, edge_index)
        x = F.relu(x)
        x = self.conv3(x, edge_index)
        x = global_mean_pool(x, batch)
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin1(x)
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin2(x)

        return x

print("Printing Model:")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = GCN(hidden_channels=64).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
print(model)
criterion = torch.nn.CrossEntropyLoss()

def train():
    model.train()

    for data in tqdm(train_loader):
        out = model(data.x, data.edge_index, data.batch)
        loss = criterion(out, data.y)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
    return loss

def test(loader):
    model.eval()
    correct = 0
    for data in loader:
        out = model(data.x, data.edge_index, data.batch)
        pred = out.argmax(dim=1)
        correct+= int((pred == data.y).sum())
    return correct/len(loader.dataset)

train_accuray = []
test_accuracy = []
train_loss = []

for epoch in range(1, 50):
    loss = train()
    train_acc = test(train_loader)
    test_acc = test(test_loader)
    train_accuray.append(train_acc)
    test_accuracy.append(test_acc)
    train_loss.append(loss)
    print(f'Epoch: {epoch:03d}, Train acc: {train_acc:.4f}, Test acc: {test_acc:.4f}, Train loss: {loss}')

plt.figure(figsize=(10,10))
plt.plot(train_accuray)
plt.plot(test_accuracy)
plt.plot(train_loss)
plt.title('Model loss: Cross-entropy loss for binary classification',fontsize=20,y=1.03)
plt.ylim(0,1)
plt.ylabel('Loss',fontsize=26)
plt.xlabel('Epoch',fontsize=26)
plt.legend(['Train Accuracy', 'Test Accuracy', 'Train Loss'], loc='upper left',fontsize=24)
plt.savefig('Accuracy_plot')

if not os.path.exists('./Saved_Models'):
    os.mkdir('./Saved_Models')
torch.save(model.state_dict(), './Saved_Models/Model.pt')
print("Model Saved")
