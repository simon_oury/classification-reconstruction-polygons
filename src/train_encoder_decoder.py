from load_data import MyOwnDataset
import torch
import torch.nn as nn
from torch.nn import Linear, ConvTranspose2d, MSELoss
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool, GAE
from torch_geometric.data import DataLoader
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib import image
import cv2


print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
train_dataset = dataset[:4500]
test_dataset = dataset[4500:5000]
image_train_dataset = []
image_test_dataset = []
for i in range (4500):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/4500" %i)
    image_train_dataset.append(image)
for i in range (4500,5000):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/500" %(i-4500))
    image_test_dataset.append(image)

print("Training dataset length: ", len(train_dataset), "\nTesting dataset length: ", len(test_dataset))
print("Training images loaded:", len(image_train_dataset), "\nTesting images loaded:", len(image_test_dataset))
print()

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=False)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)
train_images_loader = torch.utils.data.DataLoader(
    image_train_dataset, batch_size = 64
    )
test_images_loader = torch.utils.data.DataLoader(
    image_test_dataset, batch_size = 64
    )
print(f'Number of training batches created: {len(train_loader)}')
print()


class UnFlatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), 40, 6, 6)

class Encoder(torch.nn.Module):
    def __init__(self,hidden_channels):
        super(Encoder, self).__init__()
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, 32)
        self.conv3 = GCNConv(32, 64)
        self.lin1 = Linear(64, 128)

    def forward(self, x, edge_index, batch):
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = self.conv2(x, edge_index)
        x = F.relu(x)
        x = self.conv3(x, edge_index)
        x = F.relu(x)
        x = global_mean_pool(x, batch)
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin1(x)
        x = F.relu(x)


        return x


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.up = nn.Sequential(
            nn.Linear(128, 256, bias = True),
            nn.ReLU(True),
            nn.Linear(256, 512, bias = True),
            nn.ReLU(True),
            nn.Linear(512, 1440, bias = True),
            nn.ReLU(True),
            UnFlatten(),
            nn.ConvTranspose2d(40,40,kernel_size=(4,4),stride = (2,2), bias = True),
            nn.ReLU(True),
            nn.ConvTranspose2d(40,20,kernel_size = (5,5), stride = (2,2), bias = True),
            nn.ReLU(True),
            nn.ConvTranspose2d(20,1,kernel_size = (4,4), stride = (2,2), bias = True)
            )
    def forward(self, output):
        res = self.up(output)
        res = res.squeeze()
        return res


print("Printing Encoder/Decoder Model:")
model = GAE(Encoder(hidden_channels=32), Decoder())
print(model)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
criterion = MSELoss()
print(model.encoder)

def train():
    model.train()
    for data, image in zip(train_loader, train_images_loader):
        encoded = model.encode(data.x, data.edge_index, data.batch)
        out = model.decode(encoded)

        loss = criterion(out, image)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    return loss

def test():
    model.eval()

    for data, image in zip(test_loader, test_images_loader):
        encoded = model.encode(data.x, data.edge_index, data.batch)
        out = model.decode(encoded)
        test_loss = criterion(out, image)

    return test_loss

train_loss = []
test_loss = []
for epoch in range(1, 150):
    loss = 0
    loss=train()
    testloss = test()
    train_loss.append(loss)
    test_loss.append(testloss)
    if epoch%10 == 0:
        torch.save(model.encoder, 'model.pt')

    print(f'Epoch: {epoch:03d}, Train loss: {loss}, Test loss: {testloss}')

print(test_loss)
plt.figure(figsize=(10,10))
plt.plot(train_loss)
plt.plot(test_loss)
plt.title('Model loss: MSE loss for Autoencoder',fontsize=20,y=1.03)
plt.ylim(0,0.2)
plt.ylabel('Loss',fontsize=26)
plt.xlabel('Epoch',fontsize=26)
plt.legend(['Train Loss', 'Test Loss'], loc='upper left',fontsize=24)
plt.savefig('Loss_plot')

print(train_loss)

with torch.no_grad():
    for data in test_loader:
        encoded = model.encode(data.x, data.edge_index, data.batch)
        reconstruction = model.decode(encoded)
        break
with torch.no_grad():
    for batch_images in test_images_loader:
        batch_images = batch_images
        break

with torch.no_grad():
    number = 10
    plt.figure(figsize=(10, 4))
    for index in range(number):
        # display original
        ax = plt.subplot(2, number, index + 1)
        plt.imshow(batch_images[index].numpy().reshape(64, 64))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, number, index + 1 + number)
        plt.imshow(reconstruction[index].numpy().reshape(64, 64))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()
