
import numpy as np
from skimage.draw import polygon
import math
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch.autograd import Variable
import torch.nn as nn
import matplotlib.lines as mlines
import string as str
import pandas as pd
from sklearn.neighbors import KernelDensity
import cv2



def groundtruth(filename):

    def parsepolygonfile(filename):

        Polygons = []
        fileHandle = open(filename, "r")
        ListOfLines = fileHandle.readlines()
        Header = ListOfLines[0]
        NPolys = int(Header)

        def readblock(ListOfLines,StartLine):
            Polygon = []
            BlockOfLines = ListOfLines[StartLine:]

            BlockHeader = BlockOfLines[0]
            NPoints,Class = BlockHeader.split(' ')
            NPoints = int(NPoints)
            Class = int(Class)

            thisBlockOfLines = BlockOfLines[1:NPoints+1]

            ind=0
            for line in thisBlockOfLines:
                Unknown,NConn,N1,N2,x,y = line.split(' ')
                point = float(x)*32, float(y)*32
                Polygon.append(point)

                ind+=1
            Polygon = np.asarray(Polygon)
            return Polygon, Class

        for i in range(NPolys):
            P,C=readblock(ListOfLines,(i*6)+1)
            Polygons.append({'Poly':P})

        return Polygons

    def drawpolyintoemptycanvas(poly):

        img = np.zeros((64,64), dtype=float)
        R = []
        C = []
        for i in range(0,len(poly)):
            R.append(32 + poly[i,1])
            C.append(32 + poly[i,0])

        rr, cc = polygon(R, C, (64,64))
        img[rr, cc] = 1

        return img

    Polygons_permuted=parsepolygonfile(filename) # New polys:
    Polys=[P['Poly'] for P in Polygons_permuted]

    a = []

    for P in Polys:
        q = drawpolyintoemptycanvas(P)
        q = q.reshape(1,64,64)
        a.append(q)

    a = np.array(a)
    #print(len(a))
    a = torch.from_numpy(a)
    #print(a.shape)

    return a





def load_GT_data():

    print('loading data')

    g_list = groundtruth('data/POLY/shuffle_POLY.txt')

    return g_list

    print(g_list.size())

if __name__ == "__main__":
    g_list = load_GT_data()
    print(g_list.size())

    for i in range(4405, 5000):
        img = g_list[i].reshape(64,64)
        print("%d/5000"%i)
        plt.imshow(img, cmap='gray')
        plt.gca().set_axis_off()
        plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0,
                hspace = 0, wspace = 0)
        plt.margins(0,0)
        plt.savefig('data/images/image_%d.png'%i,transparent = True,bbox_inches='tight',pad_inches = 0, dpi = 13.4)
