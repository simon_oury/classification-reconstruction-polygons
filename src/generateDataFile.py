# Written by Federico Sanna
# 30/05/2019
# Script to generate input dataset to train the Graph dataset
#

# Usage Info:
# Every time that this script is run it writes into a file called
# POLY.txt a number of nodes (as specified in N_polys_to_generate) of
# the type either concave, convex, or both, as specified.
# For more flexibility the number of total graph needs to be specified
# by the user at the beginning of the file. This is to avoid that adding
# node s results in corrupting the format of the file.

# Changes made to this file by AAB
# 1. Corrected rmin to rmax swap in polygon radius argument
# 2. Corrected use of "modulo N-1" to "modulo N" for finding neighbours
#    of points to be shifted
# 3. Altered the conditions for generating convex and concave polys:
#       -- now, ALL polys are generated with a point moved 'inwards'; this creates
#          greater irregularity in the shapes, and allows the test for
#          concavity/convexity to be more challenging. Some points are moved
#          more than others (coefficient of 1 is the boundary between conv/conc)
# 4. Every call to this file generates a (hopefully, nearly) unique POLYxxxxx.txt file ID
# 5. Refactored code, and introduced separation factor (ClassSepFact). File is now easier to modify
#
# TODO (added by AAB):
#    a) Ensure point "variance" cannot be used to predict convexity/concavity
#    b) Add code so that the vertex labels can be swapped (all are numbered
#       in a clockwise or a/c sequential pattern, which means we can't test
#       that connectivity is being "understood" by the network)

import numpy as np
import torch
import random
import uuid
import os
import random
from operator import itemgetter, attrgetter
import sys
from matplotlib import pyplot as plt



def generate_polygons(rmin, rmax, n_gons, n_out):
    """    Generates n_out polygons of n_gons sides
            from a circle with a radius between rmin and rmax """
    # Creating a list of n_out random radii with values
    # between rmin and rmax (can be redundant)
    listr = np.random.ranf(n_out) * (rmax - rmin) + rmin

    # Initializing the Matrix of angles of size (n_gons, n_out)
    mat_theta = np.zeros((n_gons, n_out))
    thetanormal = [k * 2 * np.pi / n_gons for k in range(n_gons)]

    for i in range(n_out):
        mat_theta[:, i] = [np.random.normal(thetanormal[k], listr[i] / 9) for k in range(n_gons)]

    x = listr * np.cos(mat_theta)  # Xcoordinates
    y = listr * np.sin(mat_theta)  # Ycoordinates

    return (x, y) #makes a regular polygon


def generate_poly_with_variable_n_gons(n_gons, size_of_ds_poly):
    """ Generate a tuple of size size_of_ds_poly, cointaing pairs of arrays
        representing the x and y coordinates. Generates the vertices of polygons
        with n_gons corners.
        n_gons is required to be a list of integers with the numbers of corners
        of the polygons wanted to be part of the dataset.
        The dataset will be split equaly between the different types of polygons.

        Inputs:
        - n_gons (type=list): represent types of polygons that you want to
          be part of the dataset
        - size_of_ds_poly: number of polygons to be included in the dataset
        Output:
        - tuple of size 'size_of_ds_poly' cointaing elements made of two arrays
          each of size (1 x n_gons) representing the coordinates of the vertices.
        Example:
        generate_poly_with_variable_n_gons([5, 6], 3)
            a Tuple with 3 elements corresponding to:
            1 pair of arrays with the pentagon coordinates:
            [x_1,x_2,x_3,x_4,x_5]
            [y_1,y_2,y_3,y_4,y_5]

            2 pairs of arrays with the hexagons' coordinates:
            [x_11,x_12,x_13,x_14,x_15,x_16]
            [y_11,y_12,y_13,y_14,y_15,y_16]

            [x_21,x_22,x_23,x_24,x_25,x_26]
            [y_21,y_22,y_23,y_24,y_25,y_26]
        """
    # How many images with a certain number of corners
    images_per_n_gons = int(size_of_ds_poly / len(n_gons))
    # Initialise list for the vertices
    vertices = [None] * size_of_ds_poly
    # Since size_of_ds_poly/len(n_gons) could be a non-integer, we need to take
    # care of the last cases with 2 separate for loops
    # Fill the list with poly of different numbers of corners
    if len(n_gons) > 1:
        for i in range(len(n_gons) - 1):
            for j in range(images_per_n_gons):
                vertices[j + i * images_per_n_gons] = generate_polygons(0.6, 0.9, n_gons[i], 1)

    # Last one fills up until the end of the ds size
    for i in range(size_of_ds_poly - (images_per_n_gons * (len(n_gons) - 1))):
        vertices[(images_per_n_gons * (len(n_gons) - 1)) + i] = generate_polygons(0.6, 0.9, n_gons[len(n_gons) - 1], 1)
    #print(vertices[2499][0])
    return vertices

def matrix_from_polygon(vertices): #this functions gets a matrix from each of the Polygon's description
    new_matrix = np.zeros((2,5))
    for i in range(0,5):
        new_matrix[0,i]=vertices[0][i]
        new_matrix[1,i]=vertices[1][i]

    return new_matrix

def rotate_poly(vertices_matrix): #function rotates the vertices of the polygon
    new_polygons = []
    theta = np.random.uniform(-37.5,37.5)
    #theta = 0
    c = np.cos(theta)
    s = np.sin(theta)
    rotation_matrix = [[c, -s],
                       [s, c]]
    new_polygons.append(np.dot(rotation_matrix,vertices_matrix))

    return new_polygons

def point_to_class_index(y):
    _, c = y.view(-1).max(dim=0)
    return c


def perturb_poly(vertices, coefficient):
    perturbedvertices = []
    theta = []
    for v in vertices:
            # There are going to be size_of_ds_poly iterations in the loop
            # x is going to take the value of the vertex for each iteration
            # so in every loop we need to work on x
            # From vertices in the range -1,1 to vertices in the range 1,64)

            index_corner_to_be_moved = random.randint(0, v[0].shape[0]-1)
            index_corner_a = (index_corner_to_be_moved - 1) % v[0].shape[0]
            index_corner_b = (index_corner_to_be_moved + 1) % v[0].shape[0]
            c = np.array([v[0][index_corner_to_be_moved], v[1][index_corner_to_be_moved]])
            a = np.array([v[0][index_corner_a], v[1][index_corner_a]])
            b = np.array([v[0][index_corner_b], v[1][index_corner_b]])

            # Compute new position of c
            c = (((b - a) / 2) - (c - a)) * coefficient + c
            # Update position of c
            v[0][index_corner_to_be_moved] = c[0]
            v[1][index_corner_to_be_moved] = c[1]

            #rotation of the vertices of a pentagon : need to change the 5 if polygons of other size
            h = matrix_from_polygon(v)
            rot = rotate_poly(h)
            for i in range(0,2):
                for j in range(0,5):
                    v[i][j]=rot[0][i][j]

            perturbedvertices.append(v)

    return perturbedvertices


def random_rotate_poly(vertices):
    if (random.random() > 0.5):
        angle = random.random * 2 * np.pi
        center = barycenter(vertices)
        rotated = []
        for point in vertices:
            rotated.append(rotate(point, center, angle))
    else:
        rotated = vertices
    return rotated


def barycenter(vertices):
    return (np.mean(vertices, axis=0), np.mean(vertices, axis=1))


def rotate(point, center, radians):
    ox, oy = center
    px, py = point
    qx = ox + np.cos(radians) * (px - ox) - np.sin(radians) * (py - oy)
    qy = oy + np.sin(radians) * (px - ox) + np.cos(radians) * (py - oy)
    return (qx, qy)


def write_polys(canvas_size, vertices, k1, floatorint, vexcave):
    for v in vertices:
        xv = ((v[0] + 1) * (canvas_size / 2)).round()  # need to be carefull that they end up arriving to 64, not 63
        yv = ((v[1] + 1) * (canvas_size / 2)).round()
        to_be_displayed = torch.zeros((canvas_size, canvas_size, 1), dtype=torch.uint8)

        # Print number of nodes in a Graph and label of the Graph
        if vexcave == 'convex':
            f.write('5 0' + '\n')
        else:
            f.write('5 1' + '\n')

        val0 = ' 1 1 0 0'
        val1 = ' 1 0 0 0'
        val2 = ' 0 0 0 0'
        val3 = ' 0 0 0 0'
        val4 = ' 0 0 ' + str(round(random.uniform(1,5),2)) + ' ' + str(round(random.uniform(1,5),2))
        values = [val0, val1, val2, val3, val4]
        random.shuffle(values)
        # random.shuffle(k1) #comment if no permutation wanted
        i = 0
        # Populate it with ones in correspondance of vertices
        for k in range(xv.shape[0]):
            # Create 64x64 zero tensor
            base_array = torch.zeros((canvas_size, canvas_size, 1), dtype=torch.uint8)
            base_array[int(xv[k]) - 1][int(yv[k]) - 1][0] = 1
            to_be_displayed[int(xv[k]) - 1][int(yv[k]) - 1][0] = 1
            coordinates_feature = point_to_class_index(base_array)





            # Prepare the number to be written in the text file

            if k == i:

                #k = k1[0]
                if k1[0] == i:

                    connectivity = '{} {} '.format(k1[4],k1[1])
                    f.write('0 2 '+ connectivity + "{:.6f}".format(float(v[0][0])) + ' ' + "{:.6f}".format(float(v[1][0])) + values[0] + '\n')
                elif k1[1] == i:
                    connectivity = '{} {} '.format(k1[0],k1[2])
                    f.write('0 2 '+ connectivity + "{:.6f}".format(float(v[0][1])) + ' ' + "{:.6f}".format(float(v[1][1])) + values[1] + '\n')
                elif k1[2] == i:
                    connectivity = '{} {} '.format(k1[1],k1[3])
                    f.write('0 2 ' + connectivity + "{:.6f}".format(float(v[0][2])) + ' ' + "{:.6f}".format(float(v[1][2])) + values[2] + '\n')
                elif k1[3]== i:
                    connectivity = '{} {} '.format(k1[2],k1[4])
                    f.write('0 2 ' + connectivity + "{:.6f}".format(float(v[0][3])) + ' ' + "{:.6f}".format(float(v[1][3])) + values[3] + '\n')
                elif k1[4]== i:
                    connectivity = '{} {} '.format(k1[3],k1[0])
                    f.write('0 2 '+ connectivity + "{:.6f}".format(float(v[0][4])) + ' ' + "{:.6f}".format(float(v[1][4])) + values[4] + '\n')
            i +=1


if __name__ == "__main__":

    # File can be run to generate ONLY convex or concave
    # polygons.  Now refactored, so these are rather
    # less useful than in orignal version of this file
    generate_convex_poly = True
    generate_concave_poly = True
    N_polys_to_generate = 2500
    canvas_size = 64
    # ClassSepFact = float(sys.argv[1])*0.1
    ClassSepFact = 0.3
    ##print(sys.argv)
    k_per = [0,1,2,3,4]

    # Set next line to true if you want to have two float points as features for the node
    # If set True, you will have as features 2 float numbers between -1 and 1. One of them
    # represents the x coordinate, the second one represents the y coordinate.
    # If set to false, you will have a single number identifying what pixel of the figure
    # is representing the corner.
    use_two_float_cord = True

    # effectively, the main stuff starts here....
    to_be_displayed = torch.zeros((canvas_size, canvas_size, 1), dtype=torch.uint8)

    # Create Unique File Name: AAB
    unique_uid = str(uuid.uuid4())  # But this is way too long for a filename....
    unique_filename = os.path.join(__file__, "..", "..", "data", 'POLY' + unique_uid[0:5] + '.txt')
    unique_filename = os.path.abspath(unique_filename)

    # File handling
    # f = open(unique_filename, 'w')
    f = open("POLY_data.txt", 'w')
    f.write(str(2*N_polys_to_generate) + '\n')

    # The following generates two different types of polygons

    # NOTE: To reproduce Federico's results, only the concave poly vertices are "perturbed"
    # However, this is a relatively easy problem for the network to learn, and it does not
    # require to "understand" the connectivity of the polygon vertices.
    # Perturbation of the vertices of both classes makes the discrimination problem
    # harder, and also adjustable by altering the "ClassSepFact" variable

    if generate_convex_poly is True:
        vertices = generate_poly_with_variable_n_gons([5], N_polys_to_generate)
        perturbedvertices = perturb_poly(vertices, 1 - ClassSepFact)  # Federico's did not do this for convex polys


        if use_two_float_cord is False:
            write_polys(canvas_size, perturbedvertices, k_per,'int', 'convex')
        elif use_two_float_cord is True:
            write_polys(canvas_size, perturbedvertices, k_per,'float', 'convex')

    if generate_concave_poly is True:
        vertices = generate_poly_with_variable_n_gons([5], N_polys_to_generate)
        perturbedvertices = perturb_poly(vertices, 1 + ClassSepFact)  # Set ClassSepFact to 0.1 to reproduce original

        if use_two_float_cord is False:
            write_polys(canvas_size, perturbedvertices,k_per, 'int', 'concave')
        elif use_two_float_cord is True:
            write_polys(canvas_size, perturbedvertices, k_per, 'float', 'concave')

    f.close

    # Uncomment lines below if you wish to display an
    # image-based representation of the polygons.
    #to_be_displayed1 = to_be_displayed.numpy()
    #to_be_displayed1 = to_be_displayed1.reshape(64,64)
    #print(to_be_displayed1.size)
    #plt.imshow(to_be_displayed1, cmap='gray')
#plt.show()
