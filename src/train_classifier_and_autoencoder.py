from load_data import MyOwnDataset
import torch
import torch.nn as nn
from torch.nn import Linear, ConvTranspose2d, MSELoss, CrossEntropyLoss
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool, GAE
from torch_geometric.data import DataLoader
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib import image
import cv2
import os


print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
train_dataset = dataset[:4500]
test_dataset = dataset[4500:5000]
image_train_dataset = []
image_test_dataset = []
for i in range (4500):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/4500" %i)
    image_train_dataset.append(image)
for i in range (4500,5000):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/500" %(i-4500))
    image_test_dataset.append(image)


print("Training dataset length: ", len(train_dataset), "\nTesting dataset length: ", len(test_dataset))
print("Training images loaded:", len(image_train_dataset), "\nTesting images loaded:", len(image_test_dataset))
print()

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=False)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)
train_images_loader = torch.utils.data.DataLoader(
    image_train_dataset, batch_size = 64
    )
test_images_loader = torch.utils.data.DataLoader(
    image_test_dataset, batch_size = 64
    )
print(f'Number of training batches created: {len(train_loader)}')
print()



class UnFlatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), 40, 6, 6)

class Encoder(torch.nn.Module):
    def __init__(self,hidden_channels):
        super(Encoder, self).__init__()
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, 32)
        self.conv3 = GCNConv(32, 64)

    def forward(self, x, edge_index, batch):
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = self.conv2(x, edge_index)
        x = F.relu(x)
        x = self.conv3(x, edge_index)
        x = F.relu(x)
        x = global_mean_pool(x, batch)
        # x = F.dropout(x, p=0.5, training=self.training)
        x = F.relu(x)

        return x


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.up = nn.Sequential(
            nn.Linear(64, 128, bias = True),
            nn.ReLU(True),
            nn.Linear(128, 256, bias = True),
            nn.ReLU(True),
            nn.Linear(256, 512, bias = True),
            nn.ReLU(True),
            nn.Linear(512, 1440, bias = True),
            nn.ReLU(True),
            UnFlatten(),
            nn.ConvTranspose2d(40,40,kernel_size=(4,4),stride = (2,2), bias = True),
            nn.ReLU(True),
            nn.ConvTranspose2d(40,20,kernel_size = (5,5), stride = (2,2), bias = True),
            nn.ReLU(True),
            nn.ConvTranspose2d(20,1,kernel_size = (4,4), stride = (2,2), bias = True)
            )
    def forward(self, output):
        res = self.up(output)
        res = res.squeeze()
        return res

class Classifier(torch.nn.Module):
    def __init__(self):
        super(Classifier, self).__init__()
        self.lin1 = Linear(64, 128)
        self.lin2 = Linear(128, 20)
        self.lin3 = Linear(20, dataset.num_classes)

    def forward(self, x):
        x = self.lin1(x)
        x = F.relu(x)
        x = self.lin2(x)
        x = F.relu(x)
        x = self.lin3(x)

        return x


print("Printing Encoder/Decoder Model:")
autoenencoder = GAE(Encoder(hidden_channels=32), Decoder())
classifier = Classifier()
print(autoenencoder)
print(classifier)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
autoencoder = autoencoder.to(device)
classifier = classifier.to(device)
optimizer = torch.optim.Adam(autoencoder.parameters(), lr=0.001)
AEcriterion = MSELoss()
optimizer2 = torch.optim.Adam(classifier.parameters(), lr=0.01)
CLScriterion = CrossEntropyLoss()

def train():
    autoencoder.train()
    classifier.train()
    for data, image in zip(train_loader, train_images_loader):
        encoded = autoencoder.encode(data.x, data.edge_index, data.batch)
        recons = autoencoder.decode(encoded)
        classified = classifier(encoded)

        AELoss = AEcriterion(recons, image)
        CLSLoss = CLScriterion(classified, data.y)
        loss = (0.6*AELoss + 0.4*CLSLoss)
        optimizer.zero_grad()
        optimizer2.zero_grad()
        loss.backward()
        optimizer.step()
        optimizer2.step()

    return loss

def test():
    autoencoder.eval()
    classifier.eval()
    
    for data, image in zip(test_loader, test_images_loader):
        encoded = autoencoder.encode(data.x, data.edge_index, data.batch)
        recons = autoencoder.decode(encoded)
        classified = classifier(encoded)

        AEtest_loss = AEcriterion(recons, image)
        CLStest_loss = CLScriterion(classified, image)

        test_loss = (0.6*AELoss + 0.4*CLSLoss)

    return test_loss

train_loss = []
test_loss = []
for epoch in range(1, 150):
    loss = 0
    loss=train()
    testloss = test()
    train_loss.append(loss)
    # test_loss.append(testloss)
    # if epoch%10 == 0:
    #     torch.save(model.encoder, 'model.pt')

    print(f'Epoch: {epoch:03d}, Train loss: {loss}, Test loss: {testloss}')
