from load_data import MyOwnDataset
import torch
import torch.nn as nn
from torch.nn import Linear, ConvTranspose2d, MSELoss
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool, GAE
from torch_geometric.data import DataLoader
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib import image
import cv2


print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
train_dataset = dataset[:4500]
test_dataset = dataset[4500:5000]
image_train_dataset = []
image_test_dataset = []
for i in range (4500):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/4500" %i)
    image_train_dataset.append(image)
for i in range (4500,5000):
    image = cv2.imread('data/images/image_%d.png'%i)
    image = torch.tensor(image)
    image = image[:,:,1]//255
    image = image.type(torch.float32)
    print("%d/500" %(i-4500))
    image_test_dataset.append(image)

print("Training dataset length: ", len(train_dataset), "\nTesting dataset length: ", len(test_dataset))
print("Training images loaded:", len(image_train_dataset), "\nTesting images loaded:", len(image_test_dataset))
print()

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=False)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)
train_images_loader = torch.utils.data.DataLoader(
    image_train_dataset, batch_size = 64
    )
test_images_loader = torch.utils.data.DataLoader(
    image_test_dataset, batch_size = 64
    )
print(f'Number of training batches created: {len(train_loader)}')
print()
"""
Train Loader Testing
"""
# dataiter = iter(train_images_loader)
# image = dataiter.next()


# print(len(image))
# plt.imshow(image[1])
# plt.show()



class Encoder(torch.nn.Module):
    def __init__(self,hidden_channels):
        super(Encoder, self).__init__()
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, 32)
        self.conv3 = GCNConv(32, 20)
        self.lin1 = Linear(20, 20)
        self.lin2 = Linear (20, 36)

    def forward(self, x, edge_index, batch):
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = self.conv2(x, edge_index)
        x = F.relu(x)
        x = self.conv3(x, edge_index)
        x = global_mean_pool(x, batch)
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin1(x)
        x = self.lin2(x)
        return x


class Decoder(nn.Module):
    def __init__(self,regression=False):
        super(Decoder, self).__init__()
        self.up = nn.Sequential(
            nn.ConvTranspose2d(1,8,kernel_size=(4,4),stride = (2,2)),
            nn.ReLU(inplace=False),
            nn.ConvTranspose2d(8,8,kernel_size = (5,5), stride = (2,2)),
            nn.ReLU(inplace=False),
            nn.ConvTranspose2d(8,1,kernel_size = (4,4), stride = (2,2)),
            nn.ReLU(inplace=False)
            )

    def forward(self, latent):

        latent = latent.view(latent.shape[0],1,6,6)
        res = self.up(latent)
        res = res.squeeze()
        return res


print("Printing Encoder/Decoder Model:")
model = GAE(Encoder(hidden_channels=64), Decoder())
print(model)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.1)
criterion = MSELoss()



def train():
    model.train()

    for data, image in zip(train_loader, train_images_loader):
        encoded = model.encode(data.x, data.edge_index, data.batch)
        out = model.decode(encoded)
        # print("decoded: ",out.size())
        # print("image: ", image.size())
        # torch.set_printoptions(edgeitems = 64)
        # print(images)
        loss = criterion(out, image)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # print(loss)
    return loss

def test():
    model.eval()

    for data, image in zip(test_loader, test_images_loader):
        encoded = model.encode(data.x, data.edge_index, data.batch)
        out = model.decode(encoded)
        test_loss = criterion(out, image)

    return test_loss


train_loss = []
test_loss = []
for epoch in range(1, 30):
    loss = 0
    loss=train()
    train_loss.append(loss)
    print(f'Epoch: {epoch:03d}, Train loss: {loss}')

plt.figure(figsize=(5,5))
plt.plot(train_loss)
plt.plot(test_loss)
plt.title('Model loss: MSE loss for Autoencoder',fontsize=20,y=1.03)
plt.ylim(0,1)
plt.ylabel('Loss',fontsize=26)
plt.xlabel('Epoch',fontsize=26)
plt.legend('Train Accuracy', loc='upper left',fontsize=24)
plt.savefig('Loss_plot')

print(train_loss)


with torch.no_grad():
    for data in test_loader:
        encoded = model.encode(data.x, data.edge_index, data.batch)
        reconstruction = model.decode(encoded)
        break
with torch.no_grad():
    for batch_images in test_images_loader:
        batch_images = batch_images
        break

with torch.no_grad():
    number = 10
    plt.figure(figsize=(20, 4))
    for index in range(number):
        # display original
        ax = plt.subplot(2, number, index + 1)
        plt.imshow(batch_images[index].numpy().reshape(64, 64))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, number, index + 1 + number)
        plt.imshow(reconstruction[index].numpy().reshape(64, 64))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()
