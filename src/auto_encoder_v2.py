from load_data import MyOwnDataset
import torch
import torch.nn as nn
from torch.nn import Linear, ConvTranspose2d
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool, GAE
from torch_geometric.data import DataLoader
from tqdm import tqdm


print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
dataset = dataset.shuffle()
train_dataset = dataset[:4500]
test_dataset = dataset[4500:]
print("Training dataset length: ", len(train_dataset), "\nTesting dataset length: ", len(test_dataset))
print()

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=False)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)
print(f'Number of training batches created: {len(train_loader)}')
print()


class Autoencoder(torch.nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()

        self.encoder = nn.Sequential(
            GCNConv(2, 64),
            GCNConv(64, 32),
            GCNConv(32, 20),
            Linear(20, 20),
        )
        self.decoder = nn.Sequential(
            ConvTranspose2d(20, 32, kernel_size=(3,3)),
            ConvTranspose2d(32, 64, kernel_size=(3,3)),
            ConvTranspose2d(64, 2, kernel_size=(3,3)),
        )


    def forward(self, x, edge_index):
        x = self.encoder(x, edge_index)
        x = self.decoder(x)
        return x



print("Printing Encoder/Decoder Model:")
model = Autoencoder().cpu()
print(model)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

def train():
    model.train()

    for data in tqdm(train_loader):
        output = model(data.x, data.edge_index)
        loss = model.recon_loss(out, data.x)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
    return loss

for epoch in range(1, 2):
    loss=train()
    print(f'Epoch: {epoch:03d}, Train loss: {loss}')
