from load_data import MyOwnDataset
import torch
import torch.nn as nn
from torch.nn import Linear, ConvTranspose2d, MSELoss
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, global_mean_pool, GAE
from torch_geometric.data import DataLoader
from tqdm import tqdm
import numpy as np
from gt_images import load_GT_data
import matplotlib.pyplot as plt


print("Loading data....")
dataset = MyOwnDataset(root = "data/POLY/")
dataset = dataset.shuffle()
file = open("data/POLY/shuffle_POLY.txt","w")
file.write("5000")
for data in dataset:
    print(data['x'])

    x = data['x'][0][0].item()
    x2 = data['x'][0][1].item()
    x3 = data['x'][1][0].item()
    x4 = data['x'][1][1].item()
    x5 = data['x'][2][0].item()
    x6 = data['x'][2][1].item()
    x7 = data['x'][3][0].item()
    x8 = data['x'][3][1].item()
    x9 = data['x'][4][0].item()
    x10 = data['x'][4][1].item()
    x = ("%f"%x)
    x2 = ('%f'%x2)
    x3 = ('%f'%x3)
    x4 = ('%f'%x4)
    x5 = ('%f'%x5)
    x6 = ('%f'%x6)
    x7 = ('%f'%x7)
    x8 = ('%f'%x8)
    x9 = ('%f'%x9)
    x10 = ('%f'%x10)

    y = data['y'][0].item()
    y1 = ('%d'%y)

    file.write("\n5 "+y1)
    file.write("\n0 2 4 1 "+x+" "+x2)
    file.write("\n0 2 0 2 "+x3+" "+x4)
    file.write("\n0 2 1 3 "+x5+" "+x6)
    file.write("\n0 2 2 4 "+x7+" "+x8)
    file.write("\n0 2 3 0 "+x9+" "+x10)
