import torch
from torch_geometric.data import Data
from torch_geometric.data import InMemoryDataset


class MyOwnDataset(InMemoryDataset):
    def __init__(self, root, transform=None, pre_transform=None):
        super(MyOwnDataset, self).__init__(root, transform, pre_transform)
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return ['file']

    @property
    def processed_file_names(self):
        return ['data.pt']

    def download(self):
        pass

    def process(self):
        data_list = []
        file = open('/POLY_data.txt')
        Lines = file.readlines()
        # edge_index = torch.tensor([[1, 3], [1, 4], [4, 1], [4, 2], [2,4], [2,0], [0,2], [0, 3], [3,0],[3,1]], dtype=torch.long)
        edge_index = torch.tensor([[0, 4], [0, 1], [1, 0], [1, 2], [2,1], [2,3], [3,2], [3, 4], [4,0],[4,3]], dtype=torch.long)

        line = 1
        while (line <= len(Lines)-1):
            numberofNodes = int(Lines[line][0])
            label = int(Lines[line][2])
            X_values = []
            Y_values = []
            X_fixation = []
            Y_fixation = []
            X_forces = []
            Y_forces = []
            i = 7
            for j in range(numberofNodes):
                value1x = ""
                value1y = ""
                fixationx = ""
                fixationy = ""
                forcex = ""
                forcey = ""

                while (i < len(Lines[j+line+1])):
                    value1x += Lines[j+line+1][i]
                    # print("X val")
                    i+=1
                    if (Lines[j+line+1][i] == " "):
                        break
                while (i < len(Lines[j+line+1])):
                    # print("Y val")
                    value1y += Lines[j+line+1][i]
                    i+=1
                    if (Lines[j+line+1][i] == " "):
                        break
                while (i < len(Lines[j+line+1])):
                    # print("Y val")
                    fixationx += Lines[j+line+1][i]
                    i+=1
                    if (Lines[j+line+1][i] == " "):
                        break
                while (i < len(Lines[j+line+1])):
                    # print("Y val")
                    fixationy += Lines[j+line+1][i]
                    i+=1
                    if (Lines[j+line+1][i] == " "):
                        break
                while (i < len(Lines[j+line+1])):
                    # print("Y val")
                    forcex += Lines[j+line+1][i]
                    i+=1
                    if (Lines[j+line+1][i] == " "):
                        break
                while (i < len(Lines[j+line+1])):
                    # print("Y val")
                    forcey += Lines[j+line+1][i]
                    i+=1
                    

                X_values.append(float(value1x))
                Y_values.append(float(value1y))
                X_fixation.append(float(fixationx))
                Y_fixation.append(float(fixationy))
                X_forces.append(float(forcex))
                Y_forces.append(float(forcey))
                j+=1
                i=7
            # x = torch.tensor([[X_values[1],Y_values[1]], [X_values[4],Y_values[4]], [X_values[2],Y_values[2]], [X_values[0],Y_values[0]], [X_values[3],Y_values[3]]], dtype=torch.float)
            x = torch.tensor([[X_values[0],Y_values[0], X_fixation[0], Y_fixation[0], X_forces[0], Y_forces[0]], [X_values[1],Y_values[1], X_fixation[1], Y_fixation[1], X_forces[1], Y_forces[1]], [X_values[2],Y_values[2], X_fixation[2], Y_fixation[2], X_forces[2], Y_forces[2]], [X_values[3],Y_values[3], X_fixation[3], Y_fixation[3], X_forces[3], Y_forces[3]], [X_values[4],Y_values[4], X_fixation[4], Y_fixation[4], X_forces[4], Y_forces[4]]], dtype=torch.float)
            y = torch.tensor([label])
            data = Data(x=x, y=y, edge_index=edge_index.t().contiguous())
            data_list.append(data)
            line += 6


        # y = torch.tensor([[-0.71], [-0.12], [0.68], [0.45], [-0.22]], dtype=torch.float)


        if self.pre_filter is not None:
            data_list = [data for data in data_list if self.pre_filter(data)]

        if self.pre_transform is not None:
            data_list = [self.pre_transform(data) for data in data_list]

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])
